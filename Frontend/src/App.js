// Importations React
import { BrowserRouter, Route, Routes } from 'react-router-dom';

// Importations des pages
import Home from './pages/Home';
import Login from './pages/authentication/Login';
import Logout from './pages/authentication/Logout';
import Register from './pages/authentication/Register';

// Importations des components
import Header from './components/Header';
import Footer from './components/Footer';

function App() {
  return (
    <BrowserRouter>
      <Header />
      
      <Routes>
        <Route path='/' element={<Home />} />
        <Route path='/login' element={<Login />} />
        <Route path='/logout' element={<Logout />} />
        <Route path='/register' element={<Register />} />
      </Routes>

      <Footer />
    </BrowserRouter>
  );
}

export default App;