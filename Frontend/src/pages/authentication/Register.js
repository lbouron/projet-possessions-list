// Importations React
import React from 'react';

// Importation du css
import '../../css/Authentication.css';

// Autres importations
import { possessionsApiService } from '../../services/possessionsApiService';

export default class Register extends React.Component {
    render() {
        return (
            <div className='divAuth'>
                <h1 className='titreAuth'>Inscription</h1>

                <form className='formAuth' action='' method='GET'>
                    <div>
                        <label for='name'>
                            Nom d'utilisateur :
                        </label>

                        <input
                            type='text'
                            name='name'
                            id='name'
                            placeholder="Nom d'utilisateur"
                            required
                        />
                    </div>

                    <div>
                        <label for='email'>
                            Adresse email :
                        </label>

                        <input
                            type='email'
                            name='email'
                            id='email'
                            placeholder='Adresse email'
                            required
                        />
                    </div>
                    
                    <div>
                        <label for='password'>
                            Mot de passe :
                        </label>
                        
                        <input
                            type='password'
                            name='password'
                            id='password'
                            placeholder='Mot de passe'
                            required
                        />
                    </div>
                    
                    <div>
                        <label for='confirmpwd'>
                            Confirmation mot de passe :
                        </label>
                        
                        <input
                            type='password'
                            name='confirmpwd'
                            id='confirmpwd'
                            placeholder='Confirmation du mot de passe'
                            required
                        />
                    </div>
                    
                    <div>
                        <input type='submit' value="Sinscrire" />
                    </div>
                </form>
            </div>
        );
    }
}