// Importations React
import React from 'react';

// Importation du css
import '../../css/Authentication.css';

// Autres importations
import { possessionsApiService } from '../../services/possessionsApiService';

export default class Login extends React.Component {
    render() {
        return (
            <div className='divAuth'>
                <h1 className='titreAuth'>Connexion</h1>
                
                <form className='formAuth' action='' method='POST'>
                    <div>
                        <label for='name'>
                            Nom d'utilisateur / Adresse email :
                        </label>
                        
                        <input
                            type='text'
                            name='name'
                            id='name'
                            placeholder="Nom d'utilisateur / Adresse email"
                            required
                        />
                    </div>

                    <div>
                        <label for='password'>
                            Mot de passe :
                        </label>
                        
                        <input
                            type='password'
                            name='password'
                            id='password'
                            placeholder='Mot de passe'
                            required
                        />
                    </div>
                    
                    <div>
                        <input type='submit' value='Se connecter' />
                    </div>
                </form>
            </div>
        );
    }
}