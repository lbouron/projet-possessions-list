// Importations React
import { Link } from 'react-router-dom';

// Importation du css
import '../css/Header.css';

// Importation logo

const Header = () => {
    return (
        <div className='divHeader'>
            <header className='header'>
                <nav>
                    <Link className='liens' to='/'>
                        <h1 className='titre'>Liste de Possessions</h1>
                    </Link>

                    <div>
                        <Link className='liens' to='/collection'>Ma collection</Link>
                        <Link className='liens' to='/logout'>Déconnexion</Link>
                        <Link className='liens' to='/login'>Connexion</Link>
                        <Link className='liens' to='/register'>Inscription</Link>
                    </div>
                </nav>
            </header>
        </div>
    );
}

export default Header;