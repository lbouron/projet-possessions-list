// Importation du css
import '../css/Object.css';

// Importation de l'image test
import noimage from '../img/noimage.png';

const Object = ({ objet }) => {
    return (
        <div className='divObject'>
            <figure>
                {objet.image
                    ? <img src={require('../img/' + objet.image + '.jpeg')} alt={objet.nom} />
                    : <img src={noimage} alt='Aucun visuel' />
                }

                <figcaption>
                    <p>{objet.nom}</p>
                    <p>{objet.marque}</p>
                    <p>{objet.categorie}</p>
                </figcaption>
            </figure>
        </div>
    );
}

export default Object;