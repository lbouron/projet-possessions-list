// Importations React
import { Link } from 'react-router-dom';

// Importation du css
import '../css/Footer.css';

// Importation du logo

const Footer = () => {
    return (
        <div className='divFooter'>
            <footer className='footer'>
                <Link to='/' className='aFooter'>
                    Liste de Possessions
                </Link>

                <p>Design by Léana & William ©2023</p>
            </footer>
        </div>
    );
}

export default Footer;