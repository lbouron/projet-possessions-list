const Sequelize = require('sequelize');
const db = require('../services/sequelize');
const models = require('./index');

const modelDefinition = {
    id: {
        type: Sequelize.DataTypes.BIGINT,
        primaryKey: true,
        unique: true,
        allowNull: false,
        autoIncrement: true
    },
    libelle: {
        type: Sequelize.DataTypes.STRING,
        allowNull: false,
        validate: {
            len: {
                args: [1, 250],
                msg: "Le nom de la marque doit faire entre 1 et 250 caractères."
            },
            notNull: {
                msg: "L'article a forcément une marque."
            },
            notEmpty: {
                msg: "La marque de l'article doit être renseignée."
            }
        }
    },
}

let modelOptions = {
    freezeTableName: true,
    paranoid: true,
    timestamps: true
}

let marquesModel = db.define('marques', modelDefinition, modelOptions);

marquesModel.hasMany(models.articles);

module.exports = marquesModel;